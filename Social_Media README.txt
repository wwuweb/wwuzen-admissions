{\rtf1\ansi\ansicpg1252\cocoartf1265\cocoasubrtf210
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;\red38\green38\blue38;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural

\f0\fs24 \cf0 Social Media Feeds READ ME\
\
Overview:\
This is a documentation on how to obtain various social media feeds, customize it and display it on your website.\
\
Facebook Feeds\
1. Enable the Facebook-pull module.\
2. Go to configuration \'97> web services \'97> Facebook pull\
3. Register your website with Facebook developer site and obtain your\
   App ID, App Secret, the graph id of the page you want your feeds forms, \
   the type of objects you want to grab ( this case feeds) and the amount of posts you \
   want to grab. Hit save configuration. IMPORTANT!!! make sure there is no spaces in the \
   settings or else it won\'92t work.\
4. Go to Structure \'97> block  and you will find the block Facebook stream. You can add the block to any web pages you like. To add it to a custom template page simply put the following code in your page template:\
\
<?php $block = module_invoke('facebook_pull', 'block_view', 'facebook_pull_default');\
print $block['content']; ?>
\b \

\b0 \
If you want to customize the block you can override the block template in your theme folder and name the block template facebook_pull-feed.tpl.php after that you will need to flush the cache page and else.\
\
*Note if the block does not show try go to flush all cache and choose page and else. Also make sure the block configuration is set right.  ALSO, make sure there is no spaces in the settings or else it won\'92t work.\
\
Twitter\
1.Enable twitter module and its dependancies.\
2.Go to Configuration \'97> web services \'97> twitter \'97> settings\
3. Register your website with twitter as an app. Make sure to enter in the correct callback url.\
4. Enter your client key and client secret. Hit save.\
5 Go to Configuration \'97> web services \'97> twitter\
6. Click on the authenticate account button and allow the app.  \
7. Go to views and click on the tweet view.\
8. You can customize and add the view to any pages you want.\
9. You can add more twitter accounts under Configuration \'97> web services \'97> twitter \
    if you wish.\
\
Instagram Feed\
1. Enable instagram feed and its dependancies.\
2. Go to Configuration \'97> web services \'97> instagram settings.\
3. Register your website with instagram and obtain the id and secret.\
4. Enter in id and secret and access token.\
5. Go to Content \'97> Instagram \
6. Click add feed and type the user name of the account you want to get the feed from. Hit save\
7. Click manage images button on your newly added feed.\
8. Click import items.\
9. Click yes to importing feed.\
10. Go to Structure \'97> Views \'97> Instagram Feed Content.\
11. Edit the Contextual Settings by removing the filters. The images will show.\
11. Customize the view to your liking.\
\
Instagram Block\
1. Enable Instagram block\
2. Go to Configuration \'97> web services \'97> instagram block\
3. Click the link to authorize account.\
4. Log in to the account you want your feeds to be pull from.\
5. Obtain user code and access token.\
6. Fill in the user code and access token and hit save\
7. Add  \cf2 \cb1 \expnd0\expndtw0\kerning0
<?php $block = module_invoke('instagram_block', 'block_view', 'instagram_block'); print $block['content']; ?> to the template of the page OR enable the block in the page you want it to be display in.\
8. If you want the block to be different looking override the block template in your own theme by creating a file Instagram-block-image.tpl.php (also create your own css classes). Flush cache afterwards.\cf0 \cb1 \kerning1\expnd0\expndtw0 \
}