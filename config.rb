require 'zen-grids'
require 'breakpoint'
require 'sass-globbing'

sass_dir        = "src/sass"
css_dir         = "css"
images_dir      = "images"
javascripts_dir = "js"

# Assuming this theme is in sites/*/themes/THEMENAME, you can add the partials
# included with a module by uncommenting and modifying one of the lines below:
#add_import_path "../../../default/modules/FOO"
#add_import_path "../../../all/modules/FOO"
#add_import_path "../../../../modules/FOO"

disable_warnings = true
relative_assets = true
