<?php

/**
 * @file
 * Logo helper functions.
 */

/**
 * Get the header image style URL.
 *
 * @param object $node
 *   The node providing the header image field.
 *
 * @return string
 *   The image style URL of the value in the header image field.
 */
function _wwuzen_admissions_logo($node) {
  $image_style_url = NULL;
  $items = field_get_items('node', $node, 'field_header_image');

  if ($items) {
    $item = array_pop($items);

    $image_style_url = image_style_url('header_image', $item['uri']);
  }

  return $image_style_url;
}
