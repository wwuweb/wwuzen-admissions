(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.setAccordionWrapper = {

    attach: function (context, settings) {

      "use strict";
      
      var $accordion = $('.paragraphs-item-accordion .center-content');
      $accordion.prepend('<span></span>');
      
      $accordion.on("click", function() {
        $(this).find('.field-accordion-description').slideToggle(400);
        $(this).find("span").toggleClass('collapse');
      });
      
    }

  };

})(jQuery, Drupal, this, this.document);
