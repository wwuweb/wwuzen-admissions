(function($, Drupal, window, document, undefined) {

    /**
     * Description: This function allows the Calendar on the Admissions website to
     * output links that are properly formatted to work with the CGI script over
     * at applyweb. It requres Content Types set up with the Tour Type as a
     * required taxonomy term, and for the field output to be rewritten as
     * [field_tour_type] with class 'rewrite-link.'
     *
     * Dependencies: This function depends on the excellent moment.js library to
     * handle formatting and output - more info about moment.js at
     * http://momentjs.com/.
     *
     * Please contact bryce.danz@wwu.edu or brycedanz __at__ gmail.com for
     * support.
     */
    Drupal.behaviors.calendarLinks = {
        attach: function() {
            var items;
            // Process each tour item
            $('.single-day:not(.no-entry .empty) .item').each(function(item) {
                // Get the tour link for each item
                var tourLink = $(this).find('.rewrite-link');

                // Get tour date from ancestor td id DOM element
                var tourDate = $(this).closest('td').attr('id');

                /**
                 * Clean up and parse the PHP output - this is currently set up for
                 * output of form [field_tour_type]. We can't pull the tour type from
                 * the DOM because it is contained within a "title" attribute that has
                 * not been set yet when this script fires (although if we really wanted
                 * to get creative we could inject it into the DOM with tokens.)
                 */
                var stringToParse = tourLink.text();
                stringToParse = stringToParse.trim();
                stringToParse = stringToParse.split(' ');

                /**
                 * Filters out whitespace-only elements from the parse input. Super
                 * helpful here because Drupal puts in a million Divs and we only want
                 * ones with relevant information. Please see
                 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
                 * for more information
                 */
                stringToParse = stringToParse.filter(function(str) {
                    return /\S/.test(str);
                });

                tourDate = tourDate.split('-');
                // Get raw date info for formatting into a Moment object for output into the link
                var rawDay = tourDate[3];
                var rawMonth = tourDate[2];
                var rawYear = tourDate[1];

                //count the number of spans within the <td>
                var rawTime = $(this).find('span.date-display-single').html();

                // Create moment.js object
                visitDate = moment(rawYear + rawDay + rawMonth + rawTime, "YYYYDDMMh:mm a");

                // Format with moment.js
                var targetUrl = getApplyWebLink(stringToParse, visitDate);
                tourLink.replaceWith(targetUrl);
            });
        }
    }

    function getApplyWebLink(str, visitDate) {
        dateParam1Raw = visitDate.format('YYYY[-]MM[-]DD');
        dateParam2Raw = visitDate.format('dddd[+]MMMM[+]Do[,]+YYYY');
        dateParam3Raw = visitDate.format('h:mm');
        dateParam4Raw = visitDate.format('a');

        // Handle day of the week
        var visitType = str.join(' ').trim();
        var visitTypeDisplay = visitType;

        // Holds node path for WFW, SIW, WP
        var nodeId;
        var nodeUrl;

        // Rewrite tour type to conform with ApplyWeb URL parameters
        var useApplyWeb;

        switch (visitType) {
            case 'Campus Tour':
                if (visitDate.day() == 6 || visitDate.day() == 0) {
                    visitType = 'Saturday+Campus+Tour';
                } else {
                    visitType = 'Weekday+Campus+Tour';
                }
                useApplyWeb = true;
                break;
            case "Discovery Day":
                visitType = "Discovery+Days";
                useApplyWeb = true;
                break;
            case "Transfer Day":
                visitType = "Transfer+Discovery+Days";
                useApplyWeb = true;
                break;
            case "Western Fall Welcome":
                nodeId = 401;
                useApplyWeb = false;
                break;
            case "Western Preview":
                nodeId = 396;
                useApplyWeb = false;
                break;
            case "Gira en español":
                nodeId = 2373;
                useApplyWeb = false;
                break;
            case "Spring Into Western":
                nodeId = 402;
                useApplyWeb = false;
                break;
            case "Admitted Discovery Day":
              visitType = "Admitted+Student+Discovery+Days";
              useApplyWeb = true;
              break;
              
            default:
                useApplyWeb = false;
        };
        // Only adjust the url if we have applyweb logic to send it to
        if (useApplyWeb == true) {
            //generate ApplyWeb URL
            baseUrl = ' https://www.applyweb.com/cgi-bin/register?s=wwuvisit';
            visitTypeParam = 'WWU_VISIT_REG_TYPE=' + visitType;
            dateParam1 = 'WWU_VISIT_REG_DATE_TIME=' + dateParam1Raw;
            dateParam2 = 'WWU_VISIT_REG_DATE_TIME_DISPLAY=' + dateParam2Raw;
            dateParam3 = 'WWU_VISIT_REG_TIME_LABEL=' + dateParam3Raw;
            dateParam4 = '+' + dateParam4Raw;
            applyWebUrl = baseUrl + '&' + visitTypeParam + '&' + dateParam1 + '&' + dateParam2 + '&' + dateParam3 + dateParam4;
            // console.log(applyWebUrl);
            // Replace PHP output with properly formatted links
            var targetUrl = '<span><a href=\'' + applyWebUrl + '\'>' + visitTypeDisplay + '</a></span>';
        }
        /**
         * This case is for the Western Fall Welcome, Western Preview and Spring
         * into Western links. Routing logic for these can be implemented here.
         */
        else {
            nodeUrl = 'http://admissions.wwu.edu/node/' + nodeId;
            targetUrl = '<span><a href=\'' + nodeUrl + '\'>' + visitTypeDisplay + '</a></span>';
        }
        return targetUrl;
    }

})(jQuery, Drupal, this, this.document);
