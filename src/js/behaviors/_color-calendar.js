(function ($, Drupal, window, document, undefined) {

  /**
   * Sets all background colors to the stripe color and sets some text to white
   * depending on readabililty
   */
  Drupal.behaviors.colorCalendar = {

    attach: function (context, settings) {

      $('.stripe').each( function () {

        var color = $(this).css('background-color');
        $(this).parent().css('background-color', color);

        if (color == 'rgb(51, 101, 159)' || color == 'rgb(51, 137, 101)' || color == 'rgb(214, 87, 89)') {
          $(this).parent().find('a').css('color', '#fff');
        }
      });
    }

  };

})(jQuery, Drupal, this, this.document);
