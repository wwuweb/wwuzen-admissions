(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.frontPageVideoResize = {

    /**
     * Debouncing technique:
     * http://www.html5rocks.com/en/tutorials/speed/animations/
     */
    attach: function (context, settings) {
      var $videoContainer;
      var $window;
      var maxHeight;
      var ticking = false;

      function update() {
        $videoContainer.css('max-height', ((maxHeight < 400) ? 400 : maxHeight) + 'px');

        ticking = false;
      }

      function requestTick() {
        if (!ticking) {
          ticking = true;

          requestAnimationFrame(update);
        }
      }

      function resize() {
        maxHeight = $window.height() - 120;

        requestTick();
      }

      $videoContainer = $('.video-container');
      $window = $(window);

      resize();
      $window.resize(resize);
    }

  };

})(jQuery, Drupal, this, this.document);
