(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.hideNextButtonCalendar = {

    attach: function (context, settings) {
      var currentMonthRaw = moment();
      var displayMonth = moment();
      var currentMonthFetched = $('.date-heading').text().trim();
      var currentDateTestString = currentMonthRaw.format("MMMM YYYY");

      if (currentDateTestString === currentMonthFetched) {
        $('.date-prev').css("display", "none");
      }

      /**
       * We want to hide the calendar's next button if it is more than 6 months
       * in advance, so we add 5 months to the current date.
       */
      currentMonthRaw.add('months', 5).calendar();
      currentDateTestString = currentMonthRaw.format("MMMM YYYY");

      if (currentDateTestString === currentMonthFetched) {
        $('.date-next').css("display", "none");
      }
    }

  };

})(jQuery, Drupal, this, this.document);
