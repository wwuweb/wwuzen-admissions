(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.psatFormAutoSumbit = {

    attach: function (context, settings) {
      /**
       * Check that the form is fully filled out, put an error in the console if
       * the form is not filled out entirely We want this to only run on the PSAT
       * Form so we target the ID of the form
       */
      var completedField = true;

      $('form#webform-client-form-159 .required').each(function () {
        if ($(this).val() === "") {
          completedField = false;
          console.log("The field " + $(this).attr('id') + " is not filled out.");
          // If it returns that means the field is not filled out so stop executing the script
        }});

        // If the form is filled out properly, submit it once the DOM loads
        if (completedField === true) {
          $(document).ready(function () {
            $("#webform-client-form-159").submit();
          });
        }
    }

  };

})(jQuery, Drupal, this, this.document);
