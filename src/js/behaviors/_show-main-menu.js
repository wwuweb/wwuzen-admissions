(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.showMainMenu = {

    attach: function (context, settings) {
      var $mainMenu;
      var $mainMenuContainer;
      var $mainMenuParent;
      var $menuItems;
      var $submenuItems;
      var $submenuLinks;
      var $window;
      var opened;
      var timeout;

      function bindHandlers() {
        $menuItems.has('ul').children('a').attr('aria-expanded', 'false');
        $menuItems.unbind('click', clickMenuItem);
        $submenuItems.unbind('keyup', keyupMenuItem);

        if ($window.width() > 800) {
          $menuItems.click(clickMenuItem);
          $submenuItems.keyup(keyupMenuItem);
        }
      }

      function resetMainMenu() {
        if ($window.width() <= 800 && opened !== null) {
          hideMenuContainer();
          $menuItems.has('ul').children('a').attr('aria-expanded', 'false');
          $(opened).removeClass('opened');
          $(opened).children('ul').hide();
          opened = null;
        }
      }

      function showMenuContainer() {
        $mainMenuContainer.animate({
          'height': '175px'
        }, timeout);

        $mainMenuParent.animate({
          'height': '168px'
        }, timeout);
      }

      function hideMenuContainer() {
        $mainMenuContainer.animate({
          'height': '71px'
        }, {
          'duration': timeout,
          'complete': function () {
            $mainMenuContainer.css('height', 'auto');
          }
        });

        $mainMenuParent.animate({
          'height': '64px'
        }, {
          'duration': timeout,
          'complete': function () {
            $mainMenuParent.css('height', 'auto');
          }
        });
      }

      function keyupMenuItem(event) {
        var $this;

        $this = $(this);

        switch (event.which) {
          case 27: // ESCAPE
            $this.parents('.opened').removeClass('opened');
            // $this.parents('.opened').children('a').attr('aria-expanded', 'false');
            hideMenuContainer();
            $this.closest('ul').slideUp(timeout);
            opened = null;
        }
      }

      function clickMenuItem(event) {
        var $currSubmenu;
        var $opened;
        var $prevSubmenu;
        var $this;

        $this = $(this);
        $currSubmenu = $this.children('ul');

        if (!$currSubmenu.length) return;

        $menuItems.unbind('click', clickMenuItem);

        // Unbind the click handler for the duration of menu animation
        setTimeout(function () {
          $menuItems.click(clickMenuItem);
        }, timeout);

        if (opened === this) {
          $this.removeClass('opened');
          $this.children('a').attr('aria-expanded', 'false');
          hideMenuContainer();
          $currSubmenu.slideUp(timeout);
          opened = null;
        } else if (opened === null) {
          $this.addClass('opened');
          $this.children('a').attr('aria-expanded', 'true');
          showMenuContainer();
          $currSubmenu.slideDown(timeout);
          opened = this;
        } else {
          $this.addClass('opened');
          $this.children('a').attr('aria-expanded', 'true');
          $opened = $(opened);
          $opened.removeClass('opened');
          $opened.children('a').attr('aria-expanded', 'false');
          $prevSubmenu = $opened.children('ul');
          $prevSubmenu.fadeOut(timeout);
          $currSubmenu.fadeIn(timeout);
          opened = this;
        }
      }

      opened = null;
      timeout = 300;
      $window = $(window, context);
      $mainMenuContainer = $('#main-menu', context);
      $mainMenuParent = $('#main-menu .menu-name-main-menu', context);
      $mainMenu = $('#main-menu .menu-name-main-menu > .menu', context);
      $menuItems = $mainMenu.children('li');
      $submenuItems = $menuItems.find('li');
      $submenuLinks = $submenuItems.find('a');

      // EVENT HANDLERS

      // Allow default on the link to the home page by slicing the first element
      // from the selection
      $menuItems.children('a').slice(1).click(function (event) {
        event.preventDefault();
      });

      $window.resize(function() {
        resetMainMenu();
        bindHandlers();
      });

      bindHandlers();
    }

  };

})(jQuery, Drupal, this, this.document);
