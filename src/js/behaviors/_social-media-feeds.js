(function ($, Drupal, window, document, undefined) {

  /**
   * Set up the home page social media feeds.
   */
  Drupal.behaviors.socialMediaFeeds = {

    attach: function (context, settings) {

      'use strict';

      /**
       * Handle mouse and keyboard events.
       */
      var EVENTS = 'click keyUp';

      /**
       * Social media feed container elements
       */
      var $facebookFeed = $('#FB-posts');
      var $twitterFeed = $('#TW-posts');
      var $instagramFeed = $('#IG-posts');

      // Handle button activation styles
      $('.social-media-button').on(EVENTS, function (event) {
        $('.social-media-button.active').removeClass('active');
        $(event.target).addClass('active');
      });

      // Bind Facebook feed selection button
      $('#facebook-feed-select').on(EVENTS, function (event) {
        $('#slide-arrow').removeClass();
        $('#slide-arrow').addClass('left-select');

        showFeed($facebookFeed);
      });

      // Bind Twitter feed selection button
      $('#twitter-feed-select').on(EVENTS, function (event) {
        $('#slide-arrow').removeClass();
        $('#slide-arrow').addClass('middle-select');

        showFeed($twitterFeed);
      });

      // Bind Instagram feed selection button
      $('#instagram-feed-select').on(EVENTS, function (event) {
        $('#slide-arrow').removeClass();
        $('#slide-arrow').addClass('right-select');

        showFeed($instagramFeed);
      });

      // Show Facebook feed by default
      showFeed($facebookFeed);

      var breakpoints = {
        'all and (min-width: 800px)': 3,
        'all and (min-width: 500px) and (max-width: 799px)': 2,
        'all and (max-width: 499px)': 1
      };

      new Western.Slider('#FB-posts .social-media-feed', breakpoints);
      new Western.Slider('#TW-posts .social-media-feed', breakpoints);
      new Western.Slider('#IG-posts .social-media-feed-instagram-wrapper', breakpoints);
      /**
       * Show the given feed element and hide all other feeds.
       *
       * 'stop()' is called to cancel any in-progress animations so that two or
       * more feeds do not show at the same time.
       *
       * @param {Object} $feed - jQuery reference to the feed to show
       */
      function showFeed($feed) {
        $feed
        .siblings(':visible')
        .stop()
        .fadeOut({
          complete: function () {
            $feed.fadeIn();
          }
        });
      }

    }

  };

})(jQuery, Drupal, this, this.document);
