(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.stickyMenu = {

    attach: function (context, settings) {
      var $window;
      var main_menu;
      var main_menu_container;

      function sticky_navigation() {
        var cur_scroll_pos = $window.scrollTop();
        var offset_top = main_menu_container.offset().top;

        /**
         * Case: nav menu is higher on page than viewport and viewport is
         * desktop sized - make menu sticky with fixed positioning using maximum
         * fixed width for menu to avoid it stretching off the viewport and
         * looking unattractive.
         */
        if (cur_scroll_pos > offset_top) {
          main_menu_container.addClass('fixed-menu-active');
          main_menu.addClass('fixed-menu-active');
        }

        /**
         * If the user is in the top part of the page looking at the video or not
         * on a desktop, use default menu styles
         */
        else {
          main_menu_container.removeClass('fixed-menu-active');
          main_menu.removeClass('fixed-menu-active');
        }
      }

      $window = $(window);
      main_menu_container = $('#main-menu');
      main_menu = main_menu_container.children('.region-navigation');

      sticky_navigation();
      $window
        .scroll(sticky_navigation)
        .resize(sticky_navigation);
    }

  };

})(jQuery, Drupal, this, this.document);
