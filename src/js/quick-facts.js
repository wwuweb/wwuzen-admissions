// From the customize.js file
/**
 * Holds custom JavaScript for the Quick-facts view in the wwu admissions site.
 *
 */
(function (window, document, undefined) {

  'use strict';

  var categories = document.getElementsByClassName('view-grouping-header');

  for (var i = 0; i < categories.length; i++) {
    switch(categories[i].innerHTML.trim()) {
        case 'Academics':
          categories[i].className += ' category-academics';
          break;
        case 'Location':
          categories[i].className += ' category-location';
          break;
        case 'Distribution of Undergraduate Degrees':
          categories[i].className += ' category-degrees';
          break;
        case 'Student Life':
          categories[i].className += ' category-life';
          break;
        case 'Retention and Graduate Rates':
          categories[i].className += ' category-degrees';
          break;
        case 'Enrollment':
          categories[i].className += ' category-enrollment';
          break;
    }

    categories[i].innerHTML = '';
  }
})(this, this.document);
