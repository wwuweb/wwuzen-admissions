/**
 * Allows Siteimprove to collect Analytics data for admissions.wwu.edu
 */
 /*<![CDATA[*/
 (function() {
  var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
  sz.src = '//siteimproveanalytics.com/js/siteanalyze_66356517.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
 })();
 /*]]>*/
