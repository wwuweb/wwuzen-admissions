/**
* @file Code for new responsive tables (i.e. events-near-you)
* @author Jacob Shafer
* For any table-view given the .stack-table class, this script will copy the table headers along
*  the left edge of the new table format when shrinking and reaching the break point, and
*  also will remove any copies headers when the screen is widened and has reached the break point.
*/

(function ($) {
   var wasResponsive = false;


   /**
   * Returns the text stored in the deepest sub-element of a given element
   * @param {object} element : the top-most parent
   * @returns {String} : the innermost-text (should be one of the fieldnames of a table)
   */
   function getDeepestText(element) {
      if (element.children().length === 0) {
         return element.html().trim();
      } else {
         return getDeepestText(element.children().first());
      }
   }

   /**
   * Changes table from responsive to normal if a change needs to occur
   * @param {object} header : jQuery selection of the table header
   */
   function adjust(header) {
      if (header.css('display') === "none") {
         if (!wasResponsive) {
            wasResponsive = true;
            $('tbody tr').each(function () {
               var entries = $(this).find('td');
               for (var i = 0; i < entries.length; i++) {
                  var newLabel = $("<div><div/>");
                  newLabel.addClass('insertHeaderLabel').html(labels[i]);
                  newLabel.insertBefore($(entries[i]));
               }
            });
         }
      } else {
         if (wasResponsive) {
            wasResponsive = false;
            $('tbody tr').find('.insertHeaderLabel').remove();
         }
      }
   }

   var header = $('.stack-table thead tr');
   // only proceed if a .stack-table exists on the page
   if (header.length > 0) {
      // Retrieve the header labels
      var labels = [];
      header.find('th').each(function () {
         labels.push(getDeepestText($(this)));
      });

      $(window).resize(function () {
         adjust(header);
      });
      adjust(header);
   }

})(jQuery);