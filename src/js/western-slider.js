/**
 * @file Source code for Western Slider module.
 * @author Nigel Packer
 *
 * WebTech custom JavaScript utilities.
 * @namespace Western
 */
var Western = (function ($, Western, window, document, undefined) {

  'use strict';

  /**
   * A jQuery slider module. Displays an unordered list of elements as an
   * animated, horizontally scrolling slideshow.
   *
   * The slider is a styled unordered list displayed as a table, with each
   * element displayed as a table cell. This automatically ensures that items
   * have equal widths without scripting. The currently visible item is
   * determined by sliding the entire aboslutely-positioned element back and
   * forth using the left margin. Percentages are used for all dimensions, which
   * allows the slider to scale with the browser resolution.
   *
   * The slider keeps track of state by assigning an index to every item using
   * jQuery.data(). The indexing scheme is 0-based, except that 0 is always the
   * index of the first visible element. Items scrolled to the left have
   * negative indices while items scrolled to the right have positive indices.
   * This makes it programatically possible to determine the current position of
   * the slider from only the data stored in the DOM.
   *
   * Responsive behavior is provided by enquire.js, which brings CSS3 media
   * queries to JavaScript. This allows the number of items in the slider to be
   * customized based on the resolution of the browser.
   *
   * Usage:
   *
   *   new Western.Slider('#slider', {
   *     'screen and (min-width: 800px)': 3,
   *     'screen and (min-width: 400px) and (max-width: 799px)': 2,
   *     'screen and (max-width: 399px)': 1
   *   });
   *
   * This will create a slider that shows three items when the browser width is
   * 800px or above, two items below 800px, and 1 item below 40px. Note that the
   * slider may break at resolutions not covered by a breakpoint rule.
   * @module class Western/Slider
   * @requires jQuery
   * @requires Enquire
   *
   * Creates a new Western Slider instance.
   * @class
   * @param {string} selector - The element to convert into a slider.
   * @param {Object} breakpoints - Hash of media queries that map to the number
   *   of items to display in the slider at each resolution range.
   */
  Western.Slider = function (selector, breakpoints) {

    /**
     * The slider element itself, as passed in from the constructor by the user.
     * @member {JQuery} _$slider
     */
    var _$slider = $(selector);

    /**
     * The number of elements in the slider.
     * @member {number} _length
     */
    var _length = _$slider.children().length;

    /**
     * Collection of the individual slider items.
     * @member {JQuery} _$items
     */
    var _$items = _$slider.find('li');

    /**
     * Pointer to the first element in the slider.
     * @member {JQuery} _$first_item
     */
    var _$first_item = _$items.first();

    /**
     * Pointer to the last element in the slider.
     * @member {JQuery} _$last_item
     */
    var _$last_item = _$items.last();

    /**
     * The number of items to show in the slider at once.
     * @member {number} _display
     */
    var _display;

    /**
     * Display values at different screen resolutions.
     * @member {Object} _breakpoints
     */
    var _breakpoints = breakpoints;

    /**
     * The event types on which to trigger input to the slider controls.
     * @member {string} _events
     */
    var _events = 'click keyUp';

    /**
     * A wrapper element added around the base slider markup to determine height
     * and contain slider controls.
     * @member {JQuery} _$wrapper
     */
    var _$wrapper;

    /**
     * Control to advance the currently visible slider elements.
     * @member {JQuery} _$next_control
     */
    var _$next_control;

    /**
     * Control to reverse the currently visible slider elements.
     * @member {JQuery} _$prev_control
     */
    var _$prev_control;

    _init();

    /**
     * Attach the event listeners to the slider controls.
     * @method _bindControls
     */
    function _bindControls() {
      _$next_control.on(_events, _next);
      _$prev_control.on(_events, _prev);
    }

    /**
     * Calculate the width of the slider.
     * @method _calcWidth
     * @param {number} item_count - Total number of elements in the slider.
     * @param {number} items_to_show - Number of elements to show in the slider
     *   at one time.
     * @return {number}
     */
    function _calcWidth(item_count, items_to_show) {
      return item_count / items_to_show * 100 + '%';
    }

    /**
     * Calculate the left margin of the slider to set the scroll position.
     * @method _calcMargin
     * @param {number} first_index - The index of the first slider item.
     * @param {number} items_to_show - Number of elements to show in the slider
     *   at one time.
     * @return {number}
     */
    function _calcMargin(first_index, items_to_show) {
      return Math.ceil(first_index / items_to_show) * 100 + '%';
    }

    /**
     * Perform initial setup tasks for the slider.
     *
     * Generate the wrapper and control markup and place these in the needed
     * lcoations in the DOM.
     * @method _init
     */
    function _init() {
      _$wrapper = $('<div></div>', {
        'class': 'social-media-feed-inner-wrapper',
        'css' : {
          'position': 'relative'
        }
      });

      _$next_control = $('<button></button>', {
        'class': 'western-slider-control western-slider-control-next',
        'aria-role': 'button'
      });

      _$prev_control = $('<button></button>', {
        'class': 'western-slider-control western-slider-control-prev',
        'aria-role': 'button'
      });

      _$prev_control.appendTo(_$wrapper);
      _$next_control.appendTo(_$wrapper);

      // Move the slider into the wrapper
      _$wrapper.insertBefore(_$slider);
      _$slider.appendTo(_$wrapper);

      _$slider.css({
        'display': 'table',
        'left': '0',
        'list-style': 'none',
        'margin': '0',
        'padding': '0',
        'position': 'absolute',
        'table-layout': 'fixed',
        'top': '0'
      });

      _$items.css({
        'box-sizing': 'content-box',
        'display': 'table-cell',
        'padding-right': '5px',
        'padding-left': '5px',
      });

      _registerBreakpoints();
      _bindControls();
    }

    /**
     * Generate a handler to call when the given breakpoint is triggered. The
     * value of the breakpoint is bound by the closure.
     * @method _makeBreakpointHandler
     * @param {string} breakpoint - The breakpoint rule as key to the
     *   breakpoints hash.
     * @return {Function}
     */
    function _makeBreakpointHandler(breakpoint) {
      return function () {
        _display = _breakpoints[breakpoint];

        _reset();
        _update();

        _$slider.css({
          'width': _calcWidth(_length, _display),
          'margin-left': _calcMargin(_$first_item.data('slider_item_index'), _display)
        });
      };
    }

    /**
     * Advance the visible elements in the slider.
     * @method _next
     */
    function _next() {
      _update(-_display);
      _scroll();
    }

    /**
     * Reverse the visible elements in the slider.
     * @method _prev
     */
    function _prev() {
      _update(_display);
      _scroll();
    }

    /**
     * Register breakpoints with enquire.js
     * @method _registerBreakpoints
     */
    function _registerBreakpoints() {
      for (var breakpoint in _breakpoints) {
        enquire.register(breakpoint, _makeBreakpointHandler(breakpoint));
      }
    }

    /**
     * Reset the slider scroll position.
     * @method _reset
     */
    function _reset() {
      _$items.each(function (index, element) {
        $(element).removeData('slider_item_index');
      });
    }


    /**
     * Update the scroll position of the slider.
     * @method _scroll
     */
    function _scroll() {
      _unbindControls();
      _$slider.animate({
        'margin-left': _calcMargin(_$first_item.data('slider_item_index'), _display)
      }, {
        complete: _bindControls
      });
    }

    /**
     * Remove event listeners from the slider controls.
     *
     * This may be used to debounce control input.
     * @method _unbindControls
     */
    function _unbindControls() {
      _$next_control.off();
      _$prev_control.off();
    }

    /**
     * Update the slider element data.
     *
     * Updates the jQuery data containing the index of each element and sets the
     * accessibility attributes appropriately. Updates the state of the controls
     * based on the current position of the slider.
     * @method _update
     * @param {number} increment - The number of elements to advance; defaults
     *   to 0.
     */
    function _update(increment) {
      var disabled;

      increment = (typeof increment !== 'undefined') ? increment : 0;

      _$items.each(function (index, element) {
        var $element = $(element);
        var slider_item_index = $element.data('slider_item_index');

        if (slider_item_index === undefined) {
          slider_item_index = index;
        } else {
          slider_item_index += increment;
        }

        if (slider_item_index >= 0 && slider_item_index < _display) {
          $element.attr('aria-hidden', false);
          $element.find('a').removeAttr('tabindex');
        } else {
          $element.attr('aria-hidden', true);
          $element.find('a').attr('tabindex', -1);
        }

        $element.data('slider_item_index', slider_item_index);
      });

      disabled = _$first_item.data('slider_item_index') >= 0;

      _$prev_control.prop('disabled', disabled);
      _$prev_control.attr('aria-disabled', disabled);

      disabled = _$last_item.data('slider_item_index') < _display;

      _$next_control.prop('disabled', disabled);
      _$next_control.attr('aria-disabled', disabled);
    }

  };

  return Western;

})(jQuery, Western || {}, this, this.document);
