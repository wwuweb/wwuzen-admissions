<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING.
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   wwuzen_admissions_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: wwuzen_admissions_field()
 *
 *   where wwuzen_admissions is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */

require_once 'includes/logo.inc';

/**
 * Implements hook_preprocess_html().
 */
function wwuzen_admissions_preprocess_html(&$variables, $hook) {
  drupal_add_library('system', 'ui');
  drupal_add_css('https://fonts.googleapis.com/css?family=Crimson+Text:600,600i', array('type' => 'external'));
  drupal_add_css('https://fonts.googleapis.com/css?family=Muli:400,400i,600,700,800', array('type' => 'external'));
}

/**
 * Implements hook_preprocess_page().
 */
function wwuzen_admissions_preprocess_page(&$variables, $hook) {
  $theme_path = drupal_get_path('theme', 'wwuzen_admissions');

  if (isset($variables['node'])) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;

    switch ($variables['node']->nid) {

      case 2335:
      case 2336:
      case 2337:
      case 2338:
      case 2339:
      case 2340:
      case 2342:
      case 2343:
      case 2344:
        $variables['logo'] = file_create_url($theme_path . '/images/bellingham_banner.jpg');
        break;

      case 3345:
      case 395:
        $variables['logo'] = file_create_url($theme_path . '/images/ho_banner.jpg');
        break;

      case 396:
        $variables['logo'] = file_create_url($theme_path . '/images/wp_banner.jpg');
        break;

      case 399:
        $variables['logo'] = file_create_url($theme_path . '/images/wsi_banner.jpg');
        break;

      case 401:
        $variables['logo'] = file_create_url($theme_path . '/images/wfw_banner.jpg');
        break;

      case 402:
        $variables['logo'] = file_create_url($theme_path . '/images/siw_banner.jpg');
        break;

      case 593:
        $variables['logo'] = file_create_url($theme_path . '/images/icd_banner.jpg');
        break;

      case 313:
        $not_found_search = drupal_get_form('google_appliance_block_form');

        unset($not_found_search['actions']);
        unset($not_found_search['searchWestern']);
        unset($not_found_search['search_keys']['#autocomplete_path']);
        unset($not_found_search['search_keys']['#autocomplete_input']);
        unset($not_found_search['search_keys']['#attributes']['class']);

        $not_found_search['search_keys']['#attributes']['placeholder'] = 'Search...';
        $not_found_search['search_keys']['#attributes']['id'] = 'search';
        $not_found_search['search_keys']['#attributes']['type'] = 'search';

        $variables['not_found_search'] = $not_found_search;
        break;

      case 63:
        $options = array(
          'type' => 'inline',
          'scope' => 'footer',
          'group' => JS_THEME,
        );
        drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=visit&gid=2551&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
          $options);
        break;

      case 55:
        // Tuition & Expenses.
        $options = array(
          'type' => 'inline',
          'scope' => 'footer',
          'group' => JS_THEME,
        );
        drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=tuition&gid=2559&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
          $options);
        break;

        case 4107:
          // Campaign Landing Page - Confirm WWU
          $options = array(
            'type' => 'inline',
            'scope' => 'footer',
            'group' => JS_THEME,
          );
          drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "bm.adentifi.com/pixel/conv/ppt=523;g=confirm-wwu;gid=10499;ord="+ordnumber+";v=120";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
            $options);
          break;

        case 4311:
          // Campaign Landing Page - Transfer Apply
          $options = array(
            'type' => 'inline',
            'scope' => 'footer',
            'group' => JS_THEME,
          );
          drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "bm.adentifi.com/pixel/conv/ppt=1493;g=transfer_apply;gid=10676;ord="+ordnumber+";v=120";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
            $options);
          break;

        case 9:
          // Admitted Student Checklist-Freshman
          $options = array(
            'type' => 'inline',
            'scope' => 'footer',
            'group' => JS_THEME,
          );
          drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "bm.adentifi.com/pixel/conv/ppt=523;g=admitted-student-checklist;gid=10501;ord="+ordnumber+";v=120";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
            $options);
          break;

        case 471:
          // Admitted Student Checklist-Transfer
          $options = array(
            'type' => 'inline',
            'scope' => 'footer',
            'group' => JS_THEME,
          );
          drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "bm.adentifi.com/pixel/conv/ppt=1493;g=admitted-student-checklist-transfer;gid=10838;ord="+ordnumber+";v=120";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
            $options);
          break;
    }

    if ($logo = _wwuzen_admissions_logo($variables['node'])) {
      $variables['logo'] = $logo;
    }
  }

  switch (request_path()) {

    case 'bellingham':
      $variables['logo'] = file_create_url($theme_path . '/images/bellingham_banner.jpg');

      break;

    case 'visit/experience-western-honors':
    case 'visit/experience-western-honors-western-express':
      $variables['logo'] = file_create_url($theme_path . '/images/ewh_banner.jpg');

      break;

    case 'quick-facts':
      drupal_add_js($theme_path . '/js/quick-facts.min.js');

      break;

    case 'counselors':
      $options = array(
        'type' => 'inline',
        'scope' => 'footer',
        'group' => JS_THEME,
      );

      drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=counselors&gid=2553&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
        $options);

      break;

    case 'visit/parking-directions':
      $options = array(
        'type' => 'inline',
        'scope' => 'footer',
        'group' => JS_THEME,
      );

      drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=parkingdirections&gid=2554&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
        $options);

      break;

    case 'apply':
      $options = array(
        'type' => 'inline',
        'scope' => 'footer',
        'group' => JS_THEME,
      );
      drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/conv/ppt=1493;g=apply_landing_page;gid=14269;ord="+ordnumber+";v=120";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
        $options);

      break;

    case 'this-is-it':
      $variables['logo'] = file_create_url($theme_path . '/images/this_is_it_banner.jpg');

      break;

    default:
      if (drupal_is_front_page()) {
        $options = array(
          'type' => 'inline',
          'scope' => 'footer',
          'group' => JS_THEME,
        );
        drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=admissionsmain&gid=2555&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt", "");document.body.appendChild(x);',
          $options);
      }

  }

  $options = array(
    'type' => 'inline',
    'scope' => 'header',
    'group' => JS_THEME,
  );

  drupal_add_js("(function(h,o,t,j,a,r){ h.hj=h.hj||function() {(h.hj.q=h.hj.q||[]).push(arguments)}; h._hjSettings={hjid:175686,hjsv:5}; a=o.getElementsByTagName('head')[0]; r=o.createElement('script');r.async=1; r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv; a.appendChild(r); })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');",
    $options);
}

/**
 * Implements hook_preprocess_node().
 */
function wwuzen_admissions_preprocess_node(&$variables, $hook) {
  $function = __FUNCTION__ . '_' . $variables['node']->type;

  if (function_exists($function)) {
    $function($variables, $hook);
  }
}

/**
 * Implements hook_preprocess_node_TYPE().
 */
function wwuzen_admissions_preprocess_node_personnel(&$variables, $hook) {
  $email = $variables['node']->field_email['und'][0]['email'];
  $link = l($email, 'https://admissions.wwu.edu/forms/personnel-contact-form?email=' . $email);
  $variables['content']['field_email'][0]['#markup'] = $link;
}

/**
 * Implements hook_preprocess_file_entity().
 *
 * Add custom theme hook suggestions based on the field referencing the file
 * entity.
 */
function wwuzen_admissions_preprocess_file_entity(&$variables, $hook) {
  if ($variables['referencing_entity_type'] === 'paragraphs_item') {
    $elements = $variables['elements'];
    $variables['theme_hook_suggestions'][] = $elements['#entity_type'] . '__' . $elements['#bundle'] . '__' . $variables['referencing_field'];
  }
}

/**
 * Implements hook_preprocess_field().
 */
function wwuzen_admissions_preprocess_field(&$variables) {
  $element = $variables['element'];

  switch ($element['#field_name']) {

    case 'field_quick_fact_category':
      $variables['classes_array'][] = drupal_clean_css_identifier('category_' . $variables['items'][0]['#title']);

      break;

    case 'field_heading':
      $variables['classes_array'][] = drupal_clean_css_identifier($element['#entity_type'] . '_' . $element['#bundle']);
      $variables['classes_array'][] = $variables['element']['#object']->field_paragraphs_color_scheme['und'][0]['value'];

      break;

    case 'field_attribution':
      if ($element['#field_type'] === 'text_long' && $element['#items'][0]['format'] == NULL) {
        $variables['items'][0]['#markup'] = nl2br($variables['items'][0]['#markup'], FALSE);
      }

      break;

  }
}

/**
 * Implements hook_preprocess_entity().
 */
function wwuzen_admissions_preprocess_entity(&$variables) {
  $elements = $variables['elements'];

  switch ($elements['#bundle']) {

    case 'cta_image_buttons':
      $variables['classes_array'][] = drupal_clean_css_identifier('cta-buttons');

      break;

    case 'instagram_block':
      $block = module_invoke('wwu_admissions_instagram_block', 'block_view', 'wwu_admissions_instagram_block');
      $block['content']['#account'] = field_view_field($variables['entity_type'], $variables['paragraphs_item'], 'field_instagram_account_name', array('label' => 'hidden'));
      $variables['content'] = isset($block['content']) ? $block['content'] : '';

      break;

  }
}

/**
 * Implements hook_preprocess_facebook_pull_feed().
 */
function wwuzen_admissions_preprocess_facebook_pull_feed(&$variables) {
  $items = array();

  foreach ($variables['items'] as $item) {
    if ($item->from->name !== 'Western Washington University') {
      continue;
    }

    if (strpos($item->message, 'Western Alert') !== FALSE) {
      continue;
    }

    if (!in_array($item->type, array('event', 'link', 'photo', 'video'))) {
      continue;
    }

    $item->created_time = t('!time ago.', array('!time' => format_interval(time() - strtotime($item->created_time))));

    if (isset($item->message) && strlen($item->message) > 120) {
      $item->message = substr($item->message, 0, 119) . '...';
    }

    array_push($items, $item);
  }

  $variables['items'] = $items;
}

/**
 * Various scripts are excluded from the footer.
 *
 * These scripts need to be loaded in the head of the HTML page.
 *
 * @param string $javascript
 *   The array of JavaScript files that have been added to the page.
 */
function wwuzen_admissions_js_alter(&$javascript) {
  foreach ($javascript as &$value) {
    $data = $value['data'];

    if (!is_array($data)) {
      $header = ($data === 'js/google_tag.script.js') || strpos($data, 'hotjar');
    }
    else {
      $header = FALSE;
    }

    $value['scope'] = ($header) ? 'header' : 'footer';
  }
}

/**
 * Allow for menus to have links that do not go anywhere.
 *
 * This is the nolink hack: https://www.drupal.org/node/143322.
 */
function wwuzen_admissions_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  if (strpos(url($element['#href']), 'nolink')) {
    $output = '<a href="#" class="nolink">' . $element['#title'] . '</a>';
  }
  else {
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
