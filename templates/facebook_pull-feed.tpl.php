<?php

/**
 * @file
 * Template override for facebook pull feed.
 */
?>
<ul class="social-media-feed">
  <?php foreach ($items as $item): ?>
  <li class="item">
    <div class="social-media-feed-message">

      <!-- START POST MEDIA -->

      <?php if (isset($item->picture)): ?>
      <div class="social-media-media">
        <a href="<?php print $item->link; ?>">
          <img src="<?php print $item->picture; ?>" alt="picture" />
        </a>
      </div>
      <?php endif; ?>

      <!-- END POST MEDIA -->

      <!-- START POST TIME -->

      <div class="social-media-feed-time">
        <?php print $item->created_time; ?>
      </div>

      <!-- END POST TIME -->

      <!-- START POST MESSSAGE -->

      <?php if (isset($item->message)): ?>
      <div class="social-media-status-default">

        <?php if (in_array($item->type, array('event', 'link', 'video')) && isset($item->name)): ?>
          <?php print l($item->name, $item->link); ?>
          <br>
        <?php endif; ?>

        <?php print $item->message; ?>
        <br>

        <a class="social-media-show" target="_blank" href="https://www.facebook.com/westernwashingtonuniversity">Show more</a>
      </div>
      <?php endif; ?>

      <!-- END POST MESSAGE -->

    </div> <!-- end div.social-media-feed-message -->
  </li> <!-- end li.item -->
  <?php endforeach; ?>
</ul> <!-- end ul.social-media-feed -->
