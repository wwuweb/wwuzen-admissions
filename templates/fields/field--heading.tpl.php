<?php

/**
 * @file
 * Field heading template when used in the heading paragraphs bundle.
 */
?>
<h1 class="<?php print $classes;?>"<?php print $attributes; ?>>
  <div class="center-content">
    <?php foreach ($items as $delta => $item): ?>
      <?php print render($item); ?>
    <?php endforeach; ?>
  </div>
</h1>
