<?php

/**
 * @file
 * Template override for Call-To-Action Image Button files.
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($content); ?>
</div>
