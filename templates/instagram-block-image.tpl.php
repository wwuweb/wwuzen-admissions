<?php
/**
 * @file
 * Template override for Instagram Block image.
 */
?>
<li>
  <div class="social-media-feed-message">
    <div class="social-media-media">
      <a class="group" target="blank_" rel="group1" href="<?php print $href ?>">
        <img style="float: left; margin: 0 5px 5px 0px; width: <?php print $width ?>px; height: <?php print $height ?>px;" src="<?php print $src ?>">
      </a>
    </div>
    <div class="social-media-feed-time">
      <?php echo t('!time ago.', array('!time' => format_interval(time() - $post->created))); ?>
    </div>
    <div class="social-media-status-default">
      <?php echo $post->caption->text ?>
    </div>
  </div>
</li>
