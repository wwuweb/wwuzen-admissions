<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region, below the main menu (if any).
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */
?>

<!-- START PAGE -->

<div class="page">

<!--
  <div class="call-to-action">
    <div class="call-to-action-text"><h1><span>&gt;&gt; </span><a href="apply">Apply by Jan. 31</a><span> &lt;&lt;</span></h1></div>
  </div>
-->

  <!-- START HEADER -->

  <header role="banner">

    <!-- START WESTERN HEADER -->

    <section class="western-header" aria-label="University Links, Search, and Navigation">
      <div class="center-content">

        <span class="western-logo">
          <a href="http://www.wwu.edu">Western Washington University</a>
        </span>

        <?php if ($site_name): ?>
        <h1 class="site-name">
          <a href="<?php print $front_page; ?>"><?php print $site_name; ?></a>
        </h1>
        <?php endif; ?>

        <nav aria-label="University Quick Links">
          <div class="western-quick-links" aria-label="Western Quick Links">
            <button aria-pressed="false" aria-label="Toggle Quick Links">Toggle Quick Links</button>
            <ul>
              <li><a href="http://www.wwu.edu/academic_calendar" title="Calendar"><span aria-hidden="true">c</span> <span>Calendar</span></a></li>
              <li><a href="http://www.wwu.edu/directory" title="Directory"><span aria-hidden="true">d</span> <span>Directory</span></a></li>
              <li><a href="http://www.wwu.edu/index" title="Index"><span aria-hidden="true">i</span> <span>Index</span></a></li>
              <li><a href="http://www.wwu.edu/campusmaps" title="Map"><span aria-hidden="true">l</span> <span>Map</span></a></li>
              <li><a href="http://mywestern.wwu.edu" title="myWestern"><span aria-hidden="true">w</span> <span>myWestern</span></a></li>
            </ul>
          </div>
          <div class="western-search" role="search" aria-label="University and Site">
            <button aria-pressed="false" aria-label="Open the search box">Open Search</button>
            <div class="western-search-widget">
              <?php print $search_box; ?>
            </div>
          </div>
          <button class="mobile-main-nav" aria-pressed="false" aria-label="Open Mobile Main Navigation">Open Main Navigation</button>
        </nav>

      </div>
    </section>

    <!-- END WESTERN HEADER -->

    <!-- START SITE HEADER -->

    <section class="site-header" aria-label="Site Header">

      <div class="video-container">
        <div class="video-fluid-wrapper">
          <iframe title="Vimeo, Admissions hero video" src="//player.vimeo.com/video/141714455?autoplay=1&title=0&byline=0&portrait=0&loop=1" width="1280" height="720" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>

      <?php if ($site_name): ?>
      <div class="site-name">
        <p><span><?php print $site_name; ?></span></p>
      </div>
      <?php endif; ?>

      <nav class="main-nav" id="main-menu" aria-label="Main site">
        <?php print render($page['navigation']); ?>
      </nav>

      <?php print render($page['header']); ?>

    </section>

    <!-- END SITE HEADER -->

  </header>

  <!-- END HEADER -->

  <!-- START CONTENT -->

  <div class="content column">
    <?php print render($page['highlighted']); ?>
    <?php print render($title_suffix); ?>
    <?php print render($tabs); ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
    <?php endif; ?>
    <?php print render($page['content']); ?>
    <?php print render($feed_icons); ?>
  </section>

  <!-- END CONTENT -->

  <!-- START MAIN -->

  <main>

    <!-- START PANEL PROMOTE -->

    <!-- <div class="panel-promote-wrapper">
      <div class="frontpage-panel">
        <div class="panel-promote">

          <div class="header-row">
            <h2>
              Western Fall Welcome
            </h2>
            <h3>
              Join us October 27 for our biggest visit program of the year.
            </h3>
          </div>

          <div>
            <ul>
              <li>Learn all about academics and life on campus
              </li>
              <li>Explore campus and residence halls
              </li>
              <li>Gain insight into the admission process
              </li>
              <li>Meet current students and the Admissions Team
              </li>
            </ul>
          </div>
          <div class="link-button-wrapper">
             <a href="https://admissions.wwu.edu/wfw" class="link-button">Register Now</a>
          </div>
        </div>
      </div>
    </div> -->

    <!-- END PANEL PROMOTE -->

    <!-- START PANEL ATTENTION -->

    <div class="panel-attention-wrapper frontpage-panel-wrapper">
      <div class="panel-attention">

        <div class="header-row">
          <?php print render($messages); ?>
          <h2 class="panel-heading">
            <span>ACADEMIC CHOICE,</span><br>
            <span>PERSONAL ATTENTION</span>
          </h2>
        </div> <!-- end div.header-row -->

        <div class="main-row">
          <div class="left-column">

            <p class="large-number"><a href="https://admissions.wwu.edu/academics">160+</a></p>
            <p class="no-mtop">academic programs, from the classics to those you get to design yourself</p>
          </div> <!-- end div.left-column -->
          <div class="right-column">
            <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-QuoteLtBlue.png" width="70" height="70" alt="Quote" class="quote-img">
            <p class="quote-text quote-italic">
              Most of the classes are small. By the end of my first week at Western, all of my professors knew my name.”
              </p>
              <p class="quote-attribution">&#8212; Dessa Meehan</p>
          </div> <!-- end div.right-column -->
        </div> <!-- end div.main-row -->

      </div>
    </div>

    <!-- END PANEL ATTENTION -->

    <!-- START PANEL VALUE -->

    <div class="panel-value-wrapper frontpage-panel-wrapper">
      <div class="panel-value">
        <div class="center-content">

          <h2 class="panel-heading">
            <span>QUALITY EDUCATION,</span><br>
            <span>GREAT VALUE</span>
          </h2>

          <div class="column-wrapper">
            <div class="left-column">
              <a href="https://admissions.wwu.edu/tuition-expenses">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Value.png" class="img-responsive" alt="Best Value"></a>
              <p>Among the best values in the nation </p>
              <span class="citation">Kiplinger's Personal Finance and Washington Monthly magazines</span>
            </div> <!-- end div.left-column -->
            <div class="center-column">
              <a href="https://admissions.wwu.edu/academics/faculty">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-GradCap.png" class="img-responsive" alt="Small Classes"></a>
              <p>Our small class sizes give students the attention they need to succeed</p>
            </div> <!-- end div.center-column -->
            <div class="right-column">
              <a href="https://admissions.wwu.edu/academics">
              <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Ribbon.png" class="img-responsive" alt="No. 1 Public, Master's-Granting University in the Pacfic Northwest"></a>
              <p>No. 1 public, master's-granting university in the Pacific Northwest</p>
              <span class="citation">US News & World Report</span>
            </div> <!-- end div.right-column -->
          </div> <!-- end div.column-wrapper -->

          <div class="employer-row">
            <a href="https://admissions.wwu.edu/academics/outcomes">
            <p class="shout-out">
              89% of bachelor’s recipients are either employed or continuing their educations within 6 months of graduating.
            </p>
                <p class="citation" style="text-align:center;">
                  2016-2017 Graduate Outcomes Report</p></a>

          </div> <!-- end div.employer-row -->
        </div>
      </div>
    </div>

    <!-- END PANEL VALUE -->

    <!-- END PANEL LOCATION -->

    <div class="panel-location-wrapper frontpage-panel-wrapper">
      <div class="panel-location">

        <div class="location-column-left">
          <h2 class="panel-heading">
            <span>FROM THE MOUNTAINS</span><br>
            <span>TO THE BAY</span>
          </h2>
          <p>
            88,500-person coastal city with a thriving arts and music scene</p>
            <p>6 miles of trails adjacent to campus<p>
            <p>55 miles to Vancouver, BC</p>
              <p>90 miles to Seattle</p>
          </p>
            <a href="https://admissions.wwu.edu/bellingham">
          <img src="<?php print $base_path . $directory; ?>/images/front-page/Location-Map.png" class="img-responsive" width="200" height="185" alt="A map of Washington state. Bellingham is located very close to the Canadian border on the coast. It is between Vancouver BC and Seattle. Spokane, Washington is to the far east. Portland, Oregon to the South."></a>
        </div> <!-- end div.location-column-left -->

        <div class="location-column-right">
          <a href="https://admissions.wwu.edu/bellingham">
          <img src="<?php print $base_path . $directory; ?>/images/front-page/Location-Photos2.gif" class="img-responsive" alt="Bellingham with a sailboat in the bay, Western Washington University on forested Sehome Hill, and the gigantic mountain Kulshan (also known as Mount Baker) looming in the distance. Bellingham is 52 miles from Mt Baker ski area."></a>
        </div> <!-- end div.location-column-right -->

      </div>
    </div>

    <!-- END PANEL LOCATION -->

    <!-- START PANEL UNDERGRADS -->

    <div class="panel-undergrads-wrapper frontpage-panel-wrapper">
      <div class="panel-undergrads">

          <h2 class="panel-heading">
            <span>AT WESTERN, OUR FOCUS IS ON</span><br>
            <span>UNDERGRADUATES</span>
          </h2>

        <div class="column-wrapper">
          <div class="undergrad-column-left">
          <p>
            Western undergraduates work directly with professors on research that will change lives.
          </p>
          <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-QuoteDkBlue.png" class="img-responsive" alt="Quotes">
          <p class="quote-text quote-italic">
            Studying biochemistry has allowed me to continue to pursue my dream of making positive contributions to the medical field.”</p>
            <p class="quote-attribution;">&#8212; Anne d'Aquino, Biochemistry</p>
          <p>Anne is working with a professor on research that will be used as a stepping stone to ultimately alleviate the medical consequences of hemophilia A.
          </p>
        </div> <!-- end div.undergrad-column-left -->

        <div class="undergrad-column-right">
          <a href="https://admissions.wwu.edu/academics/research-design-create">
          <img src="<?php print $base_path . $directory; ?>/images/front-page/StudentProfile.jpg" class="img-responsive" alt="Student picture"></a>
        </div> <!-- end div.undergrad-column-right -->
</div>
      </div>
    </div>

    <!-- END PANEL UNDERGRADS -->

    <!-- START PANEL STUDENTS -->

    <div class="panel-students-wrapper frontpage-panel-wrapper">
      <div class="panel-students">
        <div class="center-content">

          <div class="top-row">
            <h2 class="panel-heading">
              <span>OUR STUDENTS STRIVE</span><br>
              <span>TO MAKE A DIFFERENCE IN THE WORLD</span>
            </h2>
          </div> <!-- end div.top-row -->

          <div class="left-column">
            <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-PeaceCorps.png" class="img-responsive" alt="Peace Corps" >
            <p>#2 Peace Corps volunteer-producing university among medium-sized schools</p>
          </div> <!-- end div.left-column -->

          <div class="center-column">
            <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Bulb.png" class="img-responsive" alt="Renewable Energy" >
            <p>First school in the nation to offset 100% of electrical usage with renewable energy thanks to student initiatives </p>
          </div> <!-- end div.center-column -->

          <div class="right-column">
            <img src="<?php print $base_path . $directory; ?>/images/front-page/Icon-Building.png" class="img-responsive" alt="Student Clubs and Organizations">
            <p>An active student association supports 200+ campus clubs and organizations</p>
          </div> <!-- end div.right-column -->

          <div class="row2">
            <div class="slogan">
              <p>ACTIVE MINDS CHANGING LIVES</p>
            </div>
          </div> <!-- end div.row2 -->

          <div class="row3">
            <div class="fulbright">
              <p>
                Fulbright Awards are a prestigious measure of student success. Western was named a <strong>top national producer of Fullbright Scholarship winners</strong> for 2016-2017.
              </p>
            </div>
          </div> <!-- end div.row3 -->

        </div>
      </div>
    </div>

    <!-- END PANEL STUDENTS -->



    <!-- START PANEL GET STARTED -->

    <div class="panel-get-started-wrapper frontpage-panel-wrapper clearfix">
      <div class="panel-get-started">

        <div class="header-text">
          <h2 class="panel-heading">
            <span>GET STARTED</span><br>
            <span>APPLY NOW</span>
          </h2>
        </div> <!-- end div.header-text -->

        <div class="top-row">
          <a id="freshman-button" class="custom-buttons" href="freshman" onClick="ga('send', 'event', 'FunnelLinks', 'clickedFreshman', 'Freshman and Running Start Link');">Freshman & Running Start</a>
          <a id="transfer-button" class="custom-buttons" href="transfer" onClick="ga('send', 'event', 'FunnelLinks', 'clickedTransfer', 'Transfer Link');">Transfer</a>
          <a id = "international-button" class="custom-buttons" href="international" onClick="ga('send', 'event', 'FunnelLinks', 'clickedInternational', 'International Link');">International</a>
        </div> <!-- end div.top-row -->

        <div class="bottom-row">
          <a id="returning-button" class="custom-buttons" href="returning-student" onClick="ga('send', 'event', 'FunnelLinks', 'clickedReturning', 'Returning Link');">Returning</a>
          <a id="post-bac-button" class="custom-buttons" href="post-bac" onClick="ga('send', 'event', 'FunnelLinks', 'clickedPostBaccalaureate', 'Post-baccalaureate Link');">Post-baccalaureate</a>
          <a id="extended-button" class="custom-buttons" href="extended-ed" onClick="ga('send', 'event', 'FunnelLinks', 'clickedExtendedEducation', 'Extended Education Link');">Extended Education</a>
          <a id="graduate-button" class="custom-buttons" href="http://www.wwu.edu/gradschool/" onClick="ga('send', 'event', 'FunnelLinks', 'clickedGraduateStudent', 'Graduate Student Link');">Graduate Student</a>
        </div> <!-- end div.bottom-row -->

      </div>
    </div>

    <!-- END PANEL GET STARTED -->

  </main>

  <!-- END MAIN -->

  <?php if ($secondary_menu): ?>
  <nav class="secondary-nav" aria-role="Secondary">
    <?php
      print theme('links__system_secondary_menu', array(
        'links' => $secondary_menu,
        'attributes' => array(
          'class' => array('links', 'inline', 'clearfix'),
        ),
        'heading' => array(
          'text' => $secondary_menu_heading,
          'level' => 'h2',
          'class' => array('element-invisible'),
        ),
      ));
    ?>
  </nav>
  <?php endif; ?>

  <?php
    $sidebar_first  = render($page['sidebar_first']);
    $sidebar_second = render($page['sidebar_second']);
  ?>

  <?php if ($sidebar_first || $sidebar_second): ?>
  <aside class="content-sidebar">
    <?php print $sidebar_first; ?>
    <?php print $sidebar_second; ?>
  </aside>
  <?php endif; ?>

</div> <!-- end div.page -->

<!-- END PAGE -->

<!-- START FOOTER -->

<footer role="contentinfo">
  <div class="footer-wrapper">

    <div class="footer-left">
      <?php print render($page['footer_left']); ?>
    </div>

    <div class="footer-center">
      <?php print render($page['footer_center']); ?>
      <div class="western-privacy-statement">
        <a href="http://www.wwu.edu/privacy/">Website Privacy Statement</a><br>
            <a href="https://www.wwu.edu/commitment-accessibility">Accessibility</a>
      </div>
    </div>

    <div class="footer-right" role="complementary" aria-label="WWU Contact Info">
      <h2><a href="http://www.wwu.edu">Western Washington University</a></h2>

          <div class="western-contact-info">
            <p><span aria-hidden="true" class="western-address"></span>516 High Street<br>
              <span class="western-address-city">Bellingham, WA 98225</span></p>
            <p><span aria-hidden="true" class="western-telephone"></span><a href="tel:3606503000">(360) 650-3000</a></p>
            <p><span aria-hidden="true" class="western-contact"></span><a href="http://www.wwu.edu/wwucontact/">Contact Western</a></p>
          </div>

          <div class="western-social-media">
            <ul>
              <li><a href="https://social.wwu.edu"><span aria-hidden="true" class="westernIcons-WwuSocialIcon"></span>Western Social</a></li>
              <li><a href="http://www.facebook.com/westernwashingtonuniversity"><span aria-hidden="true" class="westernIcons-FacebookIcon"></span>Facebook</a></li>
              <li><a href="http://www.flickr.com/wwu"><span aria-hidden="true" class="westernIcons-FlickrIcon"></span>Flickr</a></li>
              <li><a href="https://twitter.com/WWU"><span aria-hidden="true" class="westernIcons-TwitterIcon"></span>Twitter</a></li>
              <li><a href="http://www.youtube.com/wwu"><span aria-hidden="true" class="westernIcons-YouTubeIcon"></span>Youtube</a></li>
              <li><a href="http://news.wwu.edu/go/feed/1538/ru/atom/"><span aria-hidden="true" class="westernIcons-RSSicon"></span>RSS</a></li>
            </ul>
          </div>

      <?php print render($page['footer_right']); ?>
    </div>
  </div> <!-- end div.footer-wrapper -->
</footer>

<!-- END FOOTER -->

<!-- START PAGE BOTTOM -->

<?php print render($page['bottom']); ?>

<!-- END PAGE BOTTOM -->
