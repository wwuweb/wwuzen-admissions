<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <a href="<?php print $content['field_cta_button_link']['#items'][0]['url']; ?>">
    <?php print render($content['field_cta_button_image']); ?>
    <div class="cta-image-button-link-title"><?php print $content['field_cta_button_link']['#items'][0]['title']; ?></div>
  </a>
</div>
