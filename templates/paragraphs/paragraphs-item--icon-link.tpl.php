<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <a class="icon-link-link" href="<?php print $content['field_icon_link_link']['#items'][0]['url']; ?>">
    <?php print render($content['field_icon_link_image']); ?>
    <div class="icon-link-title"><?php print $content['field_icon_link_link']['#items'][0]['title']; ?></div>
  </a>
</div>
