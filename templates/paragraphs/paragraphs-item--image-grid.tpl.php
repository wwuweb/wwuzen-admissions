<?php

/**
 * @file
 * Override the display of the image grid paragraph bundle
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($content); ?>
</div>
