<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="center-content">
    <?php print render($content); ?>
  </div>
</div>
